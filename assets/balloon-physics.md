# System wspomagania sterowania balonem
# Legenda
- z - zewnątrz - odnosi się do gęstości powietrza, temperatury, liczby cząstek, $\dots$ - czyt. na zewnątrz balonu
- w - wewnątrz - odnosi się do gęstości powietrza, temperatury, liczby cząstek, $\dots$ - czyt. wewnątrz balonu
- b - balon/ balonu - odnosi się do masy
- 0 - początkowa wartość stała - odnosi się do ciśnienia, wysokości, temperatury
- h - wysokość nad ziemią
# Stałe fizyczne/ const.
- $\text{przyspieszenie ziemskie } g = const. = 9,81 [\frac{m}{s^2}]$
- $\text{liniowa zmiana temperatury zewnętrznej od wysokości (h) } = const. = 0,6/100 = 0,006[K/m]$
- $\text{temperatura na wysokości h[m] } =const. = (T_0- 0,006 \cdot h) [K]$
- $\text{masa balonu} = const. = 500 [kg]$
- $\text{załadunek} = const. = 300[kg]$
- $\text{masa całkowita balonu }m_b = const. = 500 + \text{załadunek} = 500+300 = 800 [kg]$
- $V_{max} = 4000 [m^3]$
- $V_{min} = 3000 [m^3]$
- $T_{max} = 130[^oC] = 403[K]$ - <span style="color:green">zmienna </span>
- $\text{wysokość początkowa }h_0=0[m]$
- $\text{utrata ciepła/ brak ogrzewania }T_{loss}=const.=-0,2[K/s]$ - <span style="color:green">zmienna </span>
- $\text{temperatura przy powierzchni ziemii }T_0 = const. = 20[^oC]=293[K]$
- $\mu = const. = 0,029 [\frac{kg}{mol}]$
- $r = const. = 287,05[\frac{J}{kg \cdot K}]$ 
- $\text{ciśnienie na powierzchni ziemii }p_0 = const. = 1013[hPa] = 101300[Pa]$
- $\text{stała Avogadra } N_A = 6,023 \cdot 10^{23} \text { cząstek}$
- $\text{1 mol powietrza }=0,024[m^3]$

# Ogólne założenia
- $V_{min} \leq V \leq V_{max} [m^3]$
- $T_z \leq T_w \leq T_{max} [K]$
- $h \geq 0 [m]$

# Wzór ogólny - wyprowadzenie
```math
F_{wyp} = gV(\rho_z - \rho_w)\\ \space \\
F_g = m_bg\\ \space \\
F_w = m_ba = gV\rho_z(1-\frac{T_z}{T_w})-F_g\\ \space \\
F_w = m_ba = gV\rho_z(1-\frac{T_z}{T_w})-m_bg\\ \space \\
m_ba=g(V\rho_z(1-\frac{T_z}{T_w})-m_b)\\ \space \\
a=\frac{gV\rho_z(1-\frac{T_z}{T_w})-m_bg}{m_b}\\ \space \\
a=g\frac{V\rho_z(1-\frac{T_z}{T_w})}{m_b}-g\\
```
Z czego otrzymujemy **ostateczną postać**:
```math
a=g(\frac{V\rho_z(1-\frac{T_z}{T_w})}{m_b}-1)
```

# Poszczególne części do wyprowadzenia
## Gęstość powietrza na zewnątrz balonu
Gęstość jest zależna od wysokości (h):
```math
\rho_z = \frac{p_z}{r \cdot T_z}\\ \space \\
p_z=p_0e^{-ch}\\ \space \\
c = \frac{\mu g}{RT_z}\\ \space \\
p_z=p_0e^{-\frac{\mu g}{RT_z}\cdot h}\\
```
Z czego otrzymujemy **ostateczną postać**:
```math
\rho_z=\frac{p_0e^{-\frac{\mu g}{RT_z}\cdot h}}{r \cdot T_z}
```
**Wyliczanie gęstości powietrza na zewnątrz balonu na danej wysokości**:
```math
\rho_z(h) = \frac{101300\cdot e^{(-\frac{0,029\cdot9,81}{8,31\cdot(293-0,006\cdot h)}\cdot h)}}{287,05 \cdot (293 - 0,006 \cdot h)} \\ \space \\
\text{gdzie h - wysokość nad ziemią}
```
Dla $h=0$, funkcja zwróci wartość $1.20443751401$.
Zaś gęstość powietrza dla $293K$ jest znana i ustalona na $1.204[\frac{kg}{m^3}]$.
```math
1.20443751401-1.204=0.00043751401\\ \space \\
\text{błąd }=|\frac{0.00043751401}{1.204}| \approx 0.000363
```
Nasz błąd jest niewielki i nieznaczący, dzięki czemu symulacja lotu balonu jest w przybliżeniu **bliska rzeczywistej**.

## Ciśnienie powietrza na wysokości
Ciśnienie jest zależne od wysokości (h):
```math
p_z = p_0e^{-\frac{\mu g}{RT_z}\cdot h}
```
**Wyliczanie ciśnienia na zewnątrz balonu na danej wysokości**:
```math
p_z(h) = 101300 \cdot e^{-\frac{0,0289644 \cdot 9,81 \cdot h}{8,31 \cdot (293-0,006 \cdot h)}} = p_w(h) \\ \space \\
\text{gdzie h - wysokość nad ziemią}
```
Dla $h=0$, funkcja zwróci wartość $101300$, czyli przyjęte $p_0$.
Zaś ciśnienie powietrza dla $293K$ jest znane i ustaone na $101300.25[Pa]$.
```math
101300 - 101300.25 = -0.25\\ \ \space \\
\text{błąd }=|\frac{-0.25}{101300.25}| \approx 2,46 \cdot 10^{-6}
```
Nasz błąd jest niewielki i nieznaczący, dzięki czemu symulacja lotu balonu jest w przybliżeniu **bliska rzeczywistej**.



## Objętość gazu wewnątrz balonu
Zakładamy, że powietrze jest gazem doskonałym. Z **równania Clapeyrona** (stan gazu doskonałego) uzyskamy:
```math
V = \frac{n_wRT_w}{p_w}
```
Potrzebujemy poznać:
- liczbę moli w balonie $n_w$,
- ciśnienie wewnątrz balonu $p_w$,
manipulując temperaturą wewnątrz balonu $T_w$.
### Liczba moli
$1$ mol powietrza = $0,024 [m^3]$
```math
1 \text{ mol} = 0,024 [m^3]\\ \space \\
x \text{ mol} = V [m^3]\\ \space \\
x \text{ mol} = \frac{V}{0,024} = n_w [mol]
```

### Ciśnienie wewnątrz
**Ciśnienie wewnątrz balonu $p_w$ jest równe z ciśnieniem na zewnątrz $p_z$ i jest zależne od wysokości $h$.**

Balon może się rozszerzać do pewnego maksymalnego pułapu $V_{max}$. 
W trakcie rozszerzania balonu wraz ze wzrostem temperatury, ciśnienie gazu wewnątrz **pozostaje niezmienne, stałe i jest równe ciśnieniu na zewnątrz $p_z$**.
Jeżeli pułap $V_{max}$ zostanie przekroczony objętość nie może dalej wzrastać $\to$ **spowodowałoby to rozerwanie balonu**.
Dlatego, w celu uniknięcia pęknięcia, **nie można dalej ogrzewać balonu**.

Analogicznie w drugą stronę:
Wraz ze spadkiem temperatury wewnątrz, balon będzie **zmniejszał swoją objętość** aż do określonego pułapu minimalnego $V_{min}$.
W przypadku osiągnięcia przez balon minimalnej objętości wymagane jest minimalne podgrzanie balonu, które pozwoli na utrzymanie minimalnej krytycznej wartości balonu.

**Dla wzrostów**:
```math
\text{jeżeli }V<V_{max} \to T_w \uparrow \Leftrightarrow V \uparrow\\ \space \\
\text{jeżeli }V=V_{max} \Leftrightarrow \text{ nie można dalej ogrzewać}\\
```
**Dla spadków**:
```math
\text{jeżeli }V>V_{min} \to T_w \downarrow \Leftrightarrow V \downarrow\\ \space \\
\text{jeżeli }V=V_{min} \Leftrightarrow \text{ balon należy podgrzać}\\
```

# Funkcja zależności wysokości od czasu
```math
h(t) = h_0+at+\frac{at^2}{2}
```
gdzie:
```math
a=g(\frac{V\rho_z(1-\frac{T_z}{T_w})}{m_b}-1) \\ \space \\
\rho_z=\frac{p_0e^{-\frac{\mu g}{RT_z}\cdot h}}{r \cdot T_z} \\ \space \\
V = \frac{n_wRT_w}{p_w} \\ \space \\
n_w = \frac{V}{0,024}\\ \space \\
p_w=p_z = p_0e^{-\frac{\mu g}{RT_z}\cdot h}\\
```

# Sprawdzenie jednostek
## Gęstość powietrza na zewnątrz
```math
\rho_z = [\frac{Pa \cdot e^{-\frac{kg \cdot m \cdot m}{mol \cdot s^2 \cdot \frac{J}{mol \cdot K}\cdot K}}}{\frac{J}{kg \cdot K}\cdot K}] =[\frac{Pa \cdot const.}{\frac{kgm^2}{s^2}\cdot\frac{1}{kg}}] = \\ \space \\=[\frac{kg}{ms^2}\cdot\frac{s^2}{m^2}] =[\frac{kg}{m^3}] = \frac mV = \rho
```
## Objętość
```math
V = \frac{nRT_w}{p_w} = [\frac{mol \cdot \frac{J}{mol \cdot K} \cdot K}{Pa} = \frac{J}{Pa}=\frac{\frac{kgm^2}{s^2}}{\frac{kg}{ms^2}}]=\\ \space \\=[\frac{kgm^2}{s^2}\cdot \frac{ms^2}{kg} = m^3] = V
```
## Temperatura
```math
1-\frac{T_z}{T_w}=[const. - \frac KK] = [const. - 1] = const.
```
## Całość wzoru
```math
a = [\frac{m}{s^2}(\frac{m^3 \cdot \frac{kg}{m^3}\cdot const.}{kg}-const.)] =[\frac{m}{s^2}] = a
```
Sprawdziliśmy i dowiedliśmy, że jednostki się zgadzają, a więc otrzymany **wynik jest podany w poprawnych jednostkach**.




