# Balloon Simulator
(This is a project for my college course)

The purpose of this project is to simulate how a hot air balloon changes altitude depending on its temperature.
All math which makes this happen is available in Polish in `balloon-physics.md`. Don't hesitate to point out some glaring errors which we missed or suggest upgrades!

This project will be published under the GNU GPL v3 license once we finish working on it.
