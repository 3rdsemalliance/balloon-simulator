import math
from dash import Dash, dcc, html, Input, Output
import pandas as pd
import plotly.express as px


def calculate_graph_data(time, height, max_temp, heat_loss):

    # stałe
    g = 9.81 #stała grawitacyjna
    p0 = 101300.0 #ciśnienie powietrza na zewnątrz balonu na wysokości h = 0, przy temperaturze zewnętrznej Tz = 293K
    mi = 0.029 #masa molowa powietrza (kg/mol)
    r = 287.05 #indywidualna stała gazowa dla powietrza 
    R = 8.31 #stała gazowa
    mol = 0.024 #objętość jednego mola powietrza (m^3)
    T0 = 293.0 #temperatura zewnętrzna przy h = 0 (K)
    Th = [T0, ]

    # stałe zmienne
    mb = 500.0 #masa balonu (tara)
    z = 300.0 #masa załadunku (netto)
    mc = mb + z #masa całkowita balonu (netto + tara)
    Vmax = 4000.0 #objętość maksymalna balonu (m^3)
    Vmin = 3000.0 #objętość minimalna balonu (m^3)

    #
    V = [Vmin, ] #objętość w czasie (m^3)
    h = [0.0, ] #wysokość balonu nad powierzchnią ziemi w czasie (m)
    a = [0.0, ] #przyspieszenie balonu w czasie (m/s^2)
    Tz = [T0, ] #temperatura na zewnątrz na obecnej wysokości balonu (K)
    Tw = [T0, ] #temperatura wewnątrz balonu (K)
    dh = [height, ] #wysokość na którą należy się wzbić
    rhoz = [(p0 * math.exp((-1*mi*g*h[0])/(R*(293-0.006*h[0]))))/(r * (293-0.006*h[0])), ] #gęstość powietrza na obecnej wysokości balonu (kg/m^3)
    pz = [p0 * math.exp((-1*mi*g*h[0])/(R*(293-0.006*h[0]))), ] #ciśnienie powietrza na obecnej wysokości balonu na jego zewnątrz (Pa)
    nw = [V[0]/mol, ] #liczba moli powietrza w balonie (1)
    pw = pz #ciśnienie wewnątrz balonu (Pa)

    # czas
    t_sim = 10000 #czas przebiegu symulacji (s)
    t = [0.0, ] #lista kwantów czasu w przebiegu symulacji (s)
    tj = 0.1 #kwant czasu, co który obliczane są wszystkie wartości (s)
    N = int(time/tj)+1 #ilość iteracji programu (1)

    # regulator
    kp = 0.025 #stała całkowania
    ti = 1000.0 #wzmocnienie całkowe
    e = [height - h[0], ] #uchyb (m)
    upi = [kp*(e[0]+(tj/ti)*sum(e)), ] #zmiana wartości temperatury w czasie wyzwalana przez regulator (K)

    for i in range (1, N):
        t.append(i*tj)
        rhoz.append((p0 * math.exp((-1*mi*g*h[-1])/(R*(T0-0.006*h[-1]))))/(r * (T0-0.006*h[-1])))
        pz.append(p0 * math.exp((-1*mi*g*h[-1])/(R*(T0-0.006*h[-1]))))
        nw.append(V[-1]/mol)
        pw = pz[i]
        Tz.append(T0-0.006*h[-1])
        dh.append(dh[-1])
        e.append(e[-1]+dh[-1]-h[-1])
        upi.append(kp*(e[-1]-e[-2]+(tj/ti)*e[-1]))
        Tw.append(min(max_temp, max(T0+upi[i]-heat_loss, Tw[-1]-heat_loss)))
        V.append(min((nw[i]*R*Tw[i])/(pw), Vmax))
        a.append((g*((V[i]*rhoz[i]*(1-Tz[i]/Tw[i]))/mc-1)))
        h.append(max(h[-1]+a[i]*tj+a[i]*tj*tj/2, 0))
    t_hours = [x/3600 for x in t]
    return pd.DataFrame({
        "Time": t_hours,
        "Height": h,
        "Temperature inside of the balloon": Tw,
        "Upi": upi,
        "Acceleration": a
    })

df = calculate_graph_data(10000, 1000, 403.0, 0.1)


website = Dash(__name__)
website.layout = html.Div(children = [
    html.A("Click to see the math which makes this possible(in Polish)", id = 'math-link', href = 'assets/balloon-physics.html'),
    html.H1("Project: Automatic altitude adjuster", id = 'page-title'),
    html.H2("About the regulator"),
    "We chose to use a PI regulator for this project.",
    html.Br(),
    "Our regulator has the following properties: ",
    html.Ul(children = [
        html.Li("Integration constant: 0.025"),
        html.Li("Integral gain: 1000")
    ]),
    html.H3("Change the values and see how the balloon sets itself on the given height!"),
    html.H5("(if the balloon fails to reach a given height, try increasing max temperature)"),
    html.Div(id = 'input-div', children = [
        html.H4("Height you want the balloon at ultimately(in meters)"),
        dcc.Input(id = 'input-height', value = 500, type = "number"),
        html.H4("Simulation time(in seconds)"),
        dcc.Input(id = 'input-time', value = 10000, type = "number"),
        html.H4("Maximal temperature of the heat source(in Kelvins)"),
        dcc.Input(id = 'input-max-temp', value = 403, type = "number"),
        html.H4("Balloon's heat loss every 0.1 second(in Kelvins)"),
        dcc.Input(id = 'input-heat-loss', value = 0.1, type = "number")
    ]),
    dcc.Graph(id = 'main-graph'),
    dcc.Checklist(id = 'graphed_values',
                  options = ['Height', 'Temperature inside of the balloon', 'Upi', 'Acceleration'],
                  value = ['Height']
    ),
    html.H4("Note about acceleration:"),
    "It can reach negative values due to how it is computed, as we graph vertical acceleration.\n",
    "The balloon is constantly influenced by gravity, so we must substract ",
    html.I("g"),
    " from its vertical acceleration at all times.",
    html.H3("Values and their respective units:"),
    html.Ul(children = [
        html.Li("Time(t): hours(h)"),
        html.Li("Height(h): meters(m)"),
        html.Li("Temperature(T): Kelvin degrees(K)"),
        html.Li("Acceleration(a): meters over seconds squared(m/(s^2))")
    ])
])

@website.callback(
    Output(component_id = 'main-graph', component_property = 'figure'),
    Input(component_id = 'graphed_values', component_property = 'value'),
    Input(component_id = 'input-time', component_property = 'value'),
    Input(component_id = 'input-height', component_property = 'value'),
    Input(component_id = 'input-max-temp', component_property = 'value'),
    Input(component_id = 'input-heat-loss', component_property = 'value'),
)
def update_graph_data(input_value, time, height, max_temp, heat_loss):
    if(time != None and height != None and max_temp != None and heat_loss != None):
        df = calculate_graph_data(time, height, max_temp, heat_loss)
    graph = px.line(df, x = "Time", y = [df[element] for element in input_value], 
                        labels = {'Time': 'Time elapsed since the beginning(h)', 'value': 'Value'}
                    )
    return graph

website.run(debug = False, host = '127.0.0.1', port = "5001")